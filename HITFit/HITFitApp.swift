//
//  HITFitApp.swift
//  HITFit
//
//  Created by rafiul hasan on 1/10/21.
//

import SwiftUI

@main
struct HITFitApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
